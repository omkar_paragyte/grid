var webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    path = require('path');
plugins = [
    new HtmlWebpackPlugin({
        "template": "./app/index.html",
        "inject": true,
        "hash": true,
        "minify": {
            "collapseWhitespace": true,
            "removeRedundantAttributes": true,
            "removeAttributeQuotes": true,
            "minifyCSS": true
        }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin( "bundle.css" )
];


var config = {
    entry: { javascript: ["./app/fbData.jsx", "webpack-hot-middleware/client"] },
    output: {
        filename: 'bundle.js',
        path: './build'
    },

    devServer: {
        inline: true,
        port: 8081
    },
    module: {
        loaders: [
		 {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: require.resolve('snapsvg'),
                loader: 'imports-loader?this=>window,fix=>module.exports=0'
            },
            {
                test: /\.json$/,
                loader: "json-loader",
            },
            { 
                test: /\.less$/, 
                loader: ExtractTextPlugin.extract('style', 'css!less') 
            },
            {
                test: /\.css$/, 
                    loader: "style-loader!css-loader" 
            },
            {
                test: /\.png$/,
                loader: "url-loader?limit=100000"
            },
            {
                test: /\.jpg$/,
                loader: "file-loader"
            },

            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml'
            }
        ],

    },
    plugins: plugins,
    resolve: {

        extensions: ['', '.jsx','.js', '.json','.css']
    }
}

module.exports = config;