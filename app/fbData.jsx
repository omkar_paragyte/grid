let React = require('react');
let ReactDOM = require('react-dom');
let _ = require('lodash');
let DbContext = require('../examples/assets/scripts/persistence.js');
let Griddle = require("../scripts/griddle.jsx");
//let Inputval=require('../examples/assets/scripts/inputval.js');
let Inputval = require('../examples/assets/scripts/inputval.js');
//let OtherPager=require('./customPageComp.jsx');
let Data1 = require('./nestedData.js');
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
require("bootstrap/dist/js/bootstrap.min.js");
let Accordion = require('react-bootstrap').Accordion;
let Panel = require('react-bootstrap').Panel;
let Col = require('react-bootstrap').Col;
let ControlLabel = require('react-bootstrap').ControlLabel;
let Collapse=require('react-collapse');
let QueryBuilder=require('./QueryBuilder.js');


//var MediaQuery = require('react-responsive');

//require("https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css");

let HeaderComponent = React.createClass({
    textOnClick: function (e) {
        e.stopPropagation();
    },

    filterText: function (e) {
        this.props.filterByColumn(e.target.value, this.props.columnName)
    },

    render: function () {
        return (
            <span>
                <input type='text'
                    onChange={this.filterText}
                    placeholder={this.props.displayName}
                    onClick={this.textOnClick}
                    />
            </span>
        );
    }
});

let ColumnComponent = React.createClass({
    getDefaultProps: function () {
        return {
            "data": null,
            "classNameProp": null,
            "keyProp": 0,
            "styleProp": '',
            "enableInlineEditing": null,
            "onDoubleClickProp": null,
            "onBlurProp": null,
            "onChangeHandler": null,
            "onChangeCallFun": null,
            "typeOfControl": "textbox",
            "onClickProp": null,
            "firstColumnAppend": '',
            "nested": false,
            "nestedComp": null,
            "columnName": null,
            "customHeaderComponent": null,
            "collapseGrid": true
        }
    },
    render: function () {
        debugger;
        // let dispNone=this.props.collapseGrid&&this.props.nested?{display:"none"}:{display:'block'};
        let targetVar = "#" + this.props.uniqueKey + this.props.columnName;
        let callapseVar = this.props.collapseGrid && this.props.nested ? "collapse" : "";

        let compForNested = this.props.nested ? 'div' : '';
        return <li> {this.props.nested ?
                <Accordion>{this.props.customHeaderComponent?
                                <this.props.customHeaderComponent displayName={this.props.columnName} />:
                    <Panel header={this.props.columnName} eventKey="a">
                     {/*<td class="panel-group" id="accordion">
                           {
                               this.props.customHeaderComponent?
                                <this.props.customHeaderComponent displayName={this.props.columnName} />
                                :
                                <b data-toggle={callapseVar} 
                                   aria-controls={this.props.uniqueKey+this.props.columnName}  
                                   data-parent="#accordion" 
                                   aria-expanded="true"  
                                   //data-target={targetVar}
                                   href={targetVar}
                                   class="collapse">
                                 {this.props.columnName}
                                </b>
                            }
                        </td>*/}
                        <td  
                            onClick={this.props.onClickProp}
                            onDoubleClick={this.props.onDoubleClickProp}
                            className={this.props.classNameProp}
                            key={this.props.keyProp}
                            style={this.props.styleProp}
                            >
                            {this.props.firstColumnAppend}
                            {
                                React.cloneElement(this.props.nestedComp, {
                                    results: _.toArray(this.props.data),
                                    enableInlineEditing: true,
                                    showTableHeading: false,
                                    showPager: false,
                                    collapseGrid: false
                                }) } </td></Panel>}
                </Accordion> :
                    <Panel>
                        <Col sm={4}>
                            <ControlLabel><b>{this.props.columnName}</b></ControlLabel>
                        </Col>
                        <Col componentClass={this.props.columnName} sm={8}>                    
                                <Inputval
                                    enableInlineEditing={this.props.enableInlineEditing}
                                    onDoubleClick={this.props.onDoubleClickProp}
                                    onBlur={this.props.onBlurProp}
                                    onChangeHandler={this.props.onChangeHandler}
                                    onChangeCallFun={this.props.onChangeCallFun}
                                    typeOfControl={this.props.typeOfControl}
                                    val={this.props.data}
                                    />                        
                        </Col>
                    </Panel>
            }

        </li>
    }
});


let RowComponent = React.createClass({
    getInitialState: function () {
        return ({});
    },
    getDefaultProps: function () {
        return {
            "onClickFun": '',
            "classNameProp": '',
            "data": null,
            "displayField":null
        }
    },

    render: function () {
        debugger;
        return      <Accordion>
                        <Panel header={this.props.displayField} >
                            <ul  style={{ listStyle: "none" }}>
                                    {this.props.data}
                            </ul>
                        </Panel>
                    </Accordion>;
       ;
        {/* <tr 
                                    onClick={this.props.onClickFun} 
                                    className={this.props.classNameProp}>
                                    {this.props.data}
                            </tr>;{/*<ul  style={{listStyle: "none"}}>{this.props.data}<hr/></ul>;
                        */}
    }
});

let RenderFBData = React.createClass({

    getInitialState: function () {
        return ({
            data: [],
            key:null,
            MaxPage:2,
            page:0
        });
    },
    rowMeta: {
        "key": "Id",
        //"displayField":"FirstName"
    },
    columnMeta: [
        {
            "columnName": "Profile",
            "visible": true,
            "Nested": true,


        },
        {
            "columnName": "Id",
            "order": 1,
            "locked": false,
            "visible": true,
            "displayName": "Candidate ID",
            "Control": "textbox",
            "Editable": true,
            "customHeaderComponent": HeaderComponent,
        },
        /*{
            "columnName": "Id",
            "order": 2,
            "locked": false,
            "visible": true,
            "displayName": "Id"
        },*/
        {
            "columnName": "Skill",
            "order": 3,
            "locked": false,
            "visible": true,
            "sortable": false,
            "customHeaderComponent": HeaderComponent,
        },
        {
            "columnName": "FirstName",
            "order": 4,
            "locked": false,
            "visible": true,
            "customHeaderComponent": HeaderComponent,

        },
        /*{
            "columnName": "_revision",
            "order": 5,
            "locked": false,
            "visible": true
        },*/
        {
            "columnName": "PhoneNum",
            "order": 2,
            "locked": false,
            "visible": true,
            "customHeaderComponent": HeaderComponent,
        },
        {
            "columnName": "Permanent",
            "order": 5,
            "locked": false,
            "visible": true,
            "Control": "checkbox"
        }
    ],

    componentWillMount: function () {
        debugger;

        this.setPageSize();
        this.setMaxPage();

       // this.setState({ data: _.toArray(Data1) });

        /*  DbContext.get(["Entities", "Candidates"],QueryBuilder.new().orderBy("key").limitToFirst(20)).then((value) => {
              debugger;
              let dataArr = _.toArray(value);
              console.log(dataArr);
              this.setState({ data: dataArr });
          }).catch((e) => { console.log(e) });*/
    },
    onChangeFun: function (e) {
        debugger;

        let Id = e.props.data ? e.props.data.Id : '';
        Id != '' ?
            DbContext.update(["Entities", "Candidates", Id], e.props.data ? e.props.data : '')
                .then(console.log("data updated successfully..."))
                .catch((e) => { console.log(e) })
            : '';
    },
    setMaxPage:function(size=5){
        this.setState({ MaxPage: Math.ceil(40000/size) });
       /* DbContext.get(["Entities", "Candidates"],).then((value) => {
            debugger;
            let dataArr = _.toArray(value);
            console.log(dataArr.length);
            this.setState({ MaxPage: Math.ceil(dataArr.length/size) });
        }).catch((e) => { console.log(e) });*/
    },
    setPage:function(number){
        debugger;
        let startKey=this.state.page<number?
                    (this.state.lastKey.length?
                            this.state.lastKey[this.state.lastKey.length-1]:
                            this.state.firstKey
                    ):
                    (
                        this.state.preKey.length?
                        this.state.preKey[this.state.preKey.length-1]:
                        this.state.firstKey
                    );
        let buttonClicked=this.state.page<number?"next":"pre";
        this.setPageSize(this.state.size,
                            QueryBuilder.new().orderBy("key").startAt(startKey).limitToFirst(this.state.size+1),
                                buttonClicked);
        this.setState({page:number});
    },
    setPageSize:function(size=5,query=QueryBuilder.new().orderBy("key").limitToFirst(size),buttonClicked="next"){
        debugger; 
        this.setState({size:size,buttonClicked:buttonClicked});
        //let query=this.state.key?QueryBuilder.new().orderBy("key").startAt(this.state.key).limitToFirst(size):QueryBuilder.new().orderBy("key").limitToFirst(size);
        this.setMaxPage(size);
        this.fetchData(query);
    },
    fetchData:function(query){
             DbContext.get(["Entities", "Candidates"],query).then((value) => {
                    debugger;
                    let dataArr = _.toArray(value);
                    let firstKey=Object.keys(value)[0];
                    let preKey=this.state.preKey?this.state.preKey: [];
                    let lastKey=this.state.lastKey?this.state.lastKey:[];
                    if(this.state.buttonClicked=="next")
                    {
                        preKey.push(this.state.firstKey?this.state.firstKey:Object.keys(value)[0])                       
                        lastKey.push(Object.keys(value).pop());

                    }else{
                        preKey.pop();
                        lastKey.pop();
                    }
                    
                    this.state.firstKey?dataArr.pop():dataArr;
                    console.log(dataArr);
                    this.setState({ 
                                    data: dataArr,
                                    firstKey:firstKey,
                                    lastKey:lastKey,
                                    preKey:preKey
                                });
            }).catch((e) => {
                            console.log(e) 
                        });
    },
    render: function () {
        return (
            <Griddle
                results={this.state.data}
                onChangeHandler={this.onChangeFun}
                enableInlineEditing={true}
                columnMetadata={this.columnMeta}
                showTableHeading={false}
                rowMetadata={this.rowMeta}
                tableClassName={this.props.tableClassName}
                showFilter={this.props.showFilter}
                showSettings={this.props.showSettings}
                //useCustomRowComponent="true"
                //customRowComponent={OtherComponent}
                //customRowComp={RowComponent} 
                //customColumnComp={ColumnComponent} +
                //useCustomPagerComponent={true}
                //customPagerComponent={OtherPager} 
                externalSetPageSize={this.setPageSize}      
                useExternal={true}   
                externalMaxPage={this.state.MaxPage}   
                externalSetPage={this.setPage}
                externalCurrentPage={this.state.page}                 
                >
                <RowComponent>
                    <ColumnComponent field="ID"></ColumnComponent>                  
                    <ColumnComponent field="Education">
                        <Griddle>
                            <RowComponent>
                                <ColumnComponent field="College"></ColumnComponent>
                                <ColumnComponent field="Percentage"></ColumnComponent>
                            </RowComponent>
                        </Griddle>
                    </ColumnComponent>                
                </RowComponent>
            </Griddle>
        );
    }
});

ReactDOM.render(
    <RenderFBData
        tableClassName="table"
        showFilter={true}
        showSettings={true} />, document.getElementById('grid-basic')
);






{        { /* 
    
    <ColumnComponent field="Current Country"></ColumnComponent>
                                <ColumnComponent field="Current Pin"></ColumnComponent>
                                <ColumnComponent field="Current State"></ColumnComponent>
    
     <ColumnComponent field="Social">
                        <Griddle>
                            <RowComponent>
                                <ColumnComponent field="Type"></ColumnComponent>
                                <ColumnComponent field="ProfileId"></ColumnComponent>
                            </RowComponent>
                        </Griddle>
                    </ColumnComponent>*/}  {/*  <ColumnComponent field="TaxId"></ColumnComponent>
                                <ColumnComponent field="Nationality"></ColumnComponent>
                                <ColumnComponent field="Gender"></ColumnComponent>*/}   {/* <ColumnComponent field="Basic Note"></ColumnComponent>*/}    /*
                  <RowComponent>
                    <ColumnComponent field="Profile">
                        <Griddle>
                            <RowComponent>
                                <ColumnComponent field="ID"></ColumnComponent>
                                <ColumnComponent field="Name"></ColumnComponent>
                                <ColumnComponent field="Skill">
                                    <Griddle>
                                        <RowComponent>
                                            <ColumnComponent field="certified"></ColumnComponent>
                                            <ColumnComponent field="notcertified"></ColumnComponent>
                                        </RowComponent>
                                       </Griddle>
                                </ColumnComponent>
                                <ColumnComponent field="PhoneNum"></ColumnComponent>
                                <ColumnComponent field="Permanent"></ColumnComponent>
                            </RowComponent>
                        </Griddle>
                    </ColumnComponent>
                    <ColumnComponent field="goal" />
                  </RowComponent>
                */
}




















/*
let OtherComponent = React.createClass({
  getDefaultProps: function(){
    return { "data": {} };
  },
  render: function(){
    return (
        <tr>
            <td>{this.props.data.Id}</td>
            <td>{this.props.data.Skill}</td>
            <td>{this.props.data.Name}</td>
            <td>{this.props.data.Permanent}</td>
            <td>{this.props.data.PhoneNum}</td>
        </tr>
        );
  }
});
*/
{//other format:-
    /*<div className="custom-row-card">
    <div className="name"><strong>{this.props.data.Id}</strong><small>{this.props.data.Skill}</small></div>
    <div>{this.props.data.Name}</div>
    <div>{this.props.data.Permanent}, {this.props.data.PhoneNum}</div>
    </div> */}

