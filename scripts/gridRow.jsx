/*
   See License / Disclaimer https://raw.githubusercontent.com/DynamicTyped/Griddle/master/LICENSE
*/
var React = require('react');
var ColumnProperties = require('./columnProperties.js');
var deep = require('./deep.js');
var isFunction = require('lodash/isFunction');
var zipObject = require('lodash/zipObject');
var assign = require('lodash/assign');
var defaults = require('lodash/defaults');
var toPairs = require('lodash/toPairs');
var without = require('lodash/without');
var Inputval = require('./inputval');
let _ = require('lodash');

//var MediaQuery = require('react-responsive');


var GridRow = React.createClass({
    getInitialState: function () {
        return ({ currContext: null });
    },
    componentWillMount: function () {
        debugger;
        let rowMeta = this.props.rowSettings.rowMetadata;
        let dispField = rowMeta && rowMeta["displayField"] ? rowMeta["displayField"] : 'ID';
        this.getDisplayField(dispField, this.props.data);
        this.setState({
            currContext: this.props.data,
            uniqueKey: this.props.data["Id"],
        });
    },
    fieldArr: [],
    getDisplayField: function (dispField, data) {
        Object.keys(data).map((item) => {
            debugger;
            if (typeof data[item] !== "object")
            { if (item === dispField) this.setState({ displayField: data[item] }); }
            else {
                this.getDisplayField(dispField, data[item]);
            }
        });


    },
    getDefaultProps: function () {
        return {
            "isChildRow": false,
            "showChildren": false,
            "data": {},
            "columnSettings": null,
            "rowSettings": null,
            "hasChildren": false,
            "useGriddleStyles": true,
            "useGriddleIcons": true,
            "isSubGriddle": false,
            "paddingHeight": null,
            "rowHeight": null,
            "parentRowCollapsedClassName": "parent-row",
            "parentRowExpandedClassName": "parent-row expanded",
            "parentRowCollapsedComponent": "▶",
            "parentRowExpandedComponent": "▼",
            "onRowClick": null,
            "multipleSelectionSettings": null,
            "enableInlineEditing": false,
            "onChangeHandler": null
        }
    },
    handleClick: function (e) {
        if (this.props.onRowClick !== null && isFunction(this.props.onRowClick)) {
            this.props.onRowClick(this, e);
        } else if (this.props.hasChildren) {
            this.props.toggleChildren();
        }
    },
    handleSelectionChange: function (e) {
        //hack to get around warning that's not super useful in this case
        return;
    },
    handleSelectClick: function (e) {
        if (this.props.multipleSelectionSettings.isMultipleSelection) {
            if (e.target.type === "checkbox") {
                this.props.multipleSelectionSettings.toggleSelectRow(this.props.data, this.refs.selected.checked);
            } else {
                this.props.multipleSelectionSettings.toggleSelectRow(this.props.data, !this.refs.selected.checked)
            }
        }
    },
    ondblClick: function (e) {


        //alert(this);
    },
    onBlurFun: function (e) {

        console.log(e)
    },
    callOnChange(d) {
        //debugger;        
        console.log(this.state.currContext);
        let currentContext = this.state.currContext;
        Object.keys(currentContext)
            .map((value) => {
                (d.props.val == "true" || d.props.val == "false") ?
                    (
                        currentContext[value] == eval(d.props.val) ?
                            currentContext[value] = eval(d.state.val) : '') :
                    (
                        currentContext[value] == d.props.val ?
                            currentContext[value] = d.state.val : '')
            });
        this.setState({ currContext: currentContext });
        this.props.onChangeHandler ? this.props.onChangeHandler(this) : '';
    },
    verifyProps: function () {
        if (this.props.columnSettings === null) {
            console.error("gridRow: The columnSettings prop is null and it shouldn't be");
        }
    },
    formatData: function (data) {
        if (typeof data === 'boolean') {
            return String(data);
        }
        return data;
    },
    render: function () {

        this.verifyProps();
        var that = this;
        var columnStyles = null;

        if (this.props.useGriddleStyles) {
            columnStyles = {
                margin: "0px",
                padding: that.props.paddingHeight + "px 5px " + that.props.paddingHeight + "px 5px",
                height: that.props.rowHeight ? this.props.rowHeight - that.props.paddingHeight * 2 + "px" : null,
                backgroundColor: "#FFF",
                borderTopColor: "#DDD",
                color: "#222"
            };
        }

        var columns = this.props.columnSettings.getColumns();

        // make sure that all the columns we need have default empty values
        // otherwise they will get clipped
        var defaultValues = zipObject(columns, []);

        // creates a 'view' on top the data so we will not alter the original data but will allow us to add default values to missing columns
        var dataView = assign({}, this.props.data);

        defaults(dataView, defaultValues);
        debugger;
        console.log(this.props);
        var data = toPairs(deep.pick(dataView, without(columns, 'children')));
        debugger;
        var nodes = data.map((col, index) => {
            // var returnValue = null;
            var meta = this.props.columnSettings.getColumnMetadataByName(col[0]);

            console.log(this.props.enableInlineEditing)
            //todo: Make this not as ridiculous looking
            var firstColAppend =
                index === 0 &&
                    this.props.hasChildren &&
                    this.props.showChildren === false &&
                    this.props.useGriddleIcons
                    ?
                    <span style={this.props.useGriddleStyles ?
                        { fontSize: "10px", marginRight: "5px" } : null}>
                        {this.props.parentRowCollapsedComponent}
                    </span>
                    :
                    index === 0 &&
                        this.props.hasChildren &&
                        this.props.showChildren &&
                        this.props.useGriddleIcons
                        ?
                        <span style={this.props.useGriddleStyles ? { fontSize: "10px" } : null}>
                            {this.props.parentRowExpandedComponent}</span>
                        : "";

            if (index === 0 && this.props.isChildRow && this.props.useGriddleStyles) {
                columnStyles = assign(columnStyles, { paddingLeft: 10 })
            }
            debugger;
            let rowTemp = this.props.childRowTemplate;
            let isColumnAvail = false;
            let ColumnName = (meta && meta.displayName) ? meta.displayName : col[0];
            let customHeaderComponent = (meta && meta.customHeaderComponent) ? meta.customHeaderComponent : null;
            let columnComp = null;

            rowTemp ? (Array.isArray(rowTemp.props.children) ? rowTemp.props.children.map((item) => {
                debugger;
                if (item.props.field == col[0]) {
                    debugger;
                    this.fieldArr.push(col[0]);
                    isColumnAvail = true;
                    let childcomp = null;


                    if (typeof col[1] == "object") {
                        debugger;
                        childcomp = item.props.children;
                        col[1] = Array.isArray(col[1]) ? col[1] : [col[1]];
                    }



                    columnComp = React.cloneElement(item, {
                        data: meta ? this.formatData(col[1]) : col[1],
                        onClickProp: this.handleClick,
                        enableInlineEditing: this.props.enableInlineEditing || (meta ? meta["Editable"] : false),
                        onDoubleClickProp: this.ondblClick,
                        keyProp: index,
                        className: meta ? meta.cssClassName : null,
                        styleProp: columnStyles,
                        onBlurProp: this.onBlurFun,
                        onChangeHandler: this.props.onChangeHandler,
                        onChangeCallFun: this.callOnChange,
                        typeOfControl: meta ? meta.Control : '',
                        firstColumnAppend: firstColAppend,
                        nested: childcomp ? true : false,
                        nestedComp: childcomp,
                        columnName: ColumnName,
                        collapseGrid: this.props.collapseGrid,
                        customHeaderComponent: customHeaderComponent,
                        uniqueKey: this.state.uniqueKey
                    }
                    );
                }
            }) : '') : '';

            if (!isColumnAvail) { return null; }
            /*return <this.props.customColumnComp data={col[1]}/>*/
            console.log(meta);
            meta ? (meta["Control"] = meta.Control ? meta.Control : "textbox") : '';
            if (this.props.columnSettings.hasColumnMetadata() && typeof meta !== 'undefined' && meta !== null) {
                if (typeof meta.customComponent !== 'undefined' && meta.customComponent !== null) {
                    //  debugger;
                    var customComponent = <meta.customComponent data={col[1]} rowData={dataView} metadata={meta} />;
                    return meta["visible"] ?
                        <td
                            onClick={this.handleClick}
                            onDoubleClick={this.ondblClick.bind(null, this) }
                            className={meta.cssClassName}
                            key={index}
                            style={columnStyles}>{customComponent}</td>
                        : null;
                    {/* <td onClick={this.handleClick}
                      onDoubleClick={this.ondblClick.bind(null,this)}  
                      className={meta.cssClassName} 
                      key={index} 
                      style={columnStyles}>
                               <Inputval
                                        enableInlineEditing={this.props.enableInlineEditing}  
                                        onDoubleClick={this.ondblClick}
                                        onBlur={this.onBlurFun.bind(this)}
                                        onChangeHandler={this.props.onChangeHandler} 
                                        onChangeCallFun={this.callOnChange}   
                                        typeOfControl={meta?meta.Control:''}                                    
                                        val={customComponent} />
                                </td>:null;*/}
                } else {
                    debugger;

                    //return meta["visible"]?<this.props.customColumnComp data={col[1]}/>:null;
                    return meta["visible"] ? columnComp : null;
                    {/*<this.props.customColumnComp 
                                            data={this.formatData(col[1])}
                                            onClickProp={this.handleClick} 
                                            enableInlineEditing={this.props.enableInlineEditing}
                                            onDoubleClickProp={this.ondblClick.bind(null,this)}
                                            classNameProp={meta.cssClassName}
                                            keyProp={index} 
                                            styleProp={columnStyles}
                                            onBlurProp={this.onBlurFun.bind(this)}
                                            onChangeHandler={this.props.onChangeHandler} 
                                            onChangeCallFun={this.callOnChange}
                                            typeOfControl={meta?meta.Control:''}
                                            >{firstColAppend}</this.props.customColumnComp >:null;*/}
                    {/*    <td
                                            onClick={this.handleClick} 
                                            onDoubleClick={this.ondblClick.bind(null,this)}
                                            className={meta.cssClassName}
                                            key={index} style={columnStyles}>
                                                {firstColAppend}
                                                <Inputval
                                                    enableInlineEditing={this.props.enableInlineEditing}  
                                                    onDoubleClick={this.ondblClick}
                                                    onBlur={this.onBlurFun.bind(this)}
                                                    onChangeHandler={this.props.onChangeHandler} 
                                                    onChangeCallFun={this.callOnChange}
                                                    onDoubleClick={this.ondblClick.bind(null,this)} 
                                                    typeOfControl={meta?meta.Control:''}
                                                    val={this.formatData(col[1])}
                                                />
                                        </td>:null;*/}
                }
            }
            debugger;

            return columnComp;

            {/*<this.props.customColumnComp 
                                                        data={col[1]}
                                                        onClickProp={this.handleClick} 
                                                        enableInlineEditing={this.props.enableInlineEditing}
                                                        onDoubleClickProp={this.ondblClick.bind(null,this)}
                                                        keyProp={index} 
                                                        styleProp={columnStyles}
                                                        onBlurProp={this.onBlurFun.bind(this)}
                                                        
                                                        onChangeHandler={this.props.onChangeHandler.bind(null,this)}
                                                        onChangeCallFun={this.callOnChange}
                                                        typeOfControl={meta?meta.Control:''}
                                                        >{firstColAppend}</this.props.customColumnComp >; 
                    */}



            {/*return (returnValue || <this.props.customColumnComp 
                                                        data={col[1]} />
                                                        onClickProp={this.handleClick} 
                                                        onDoubleClickProp={this.ondblClick.bind(null,this)}
                                                        classNameProp={meta.cssClassName}
                                                        key={index} 
                                                        styleProp={columnStyles}
                                                        onBlurProp={this.onBlurFun.bind(this)}
                                                        enableInlineEditing={this.props.enableInlineEditing}
                                                        onChangeHandler={this.props.onChangeHandler.bind(null,this)}
                                                        onChangeCallFun={this.callOnChange}
                                                        typeOfControl={meta?meta.Control:''}
                                                        >{firstColAppend}</this.props.customColumnComp >
                                            /*  (<td 
                                                    onClick={this.handleClick}
                                                    onDoubleClick={this.ondblClick.bind(null,this)} 
                                                    onBlur={this.onBlurFun.bind(this)}
                                                    key={index}
                                                    style={columnStyles}
                                                >
                                                    {firstColAppend}
                                                    <Inputval
                                                        enableInlineEditing={this.props.enableInlineEditing}
                                                        onChangeHandler={this.props.onChangeHandler.bind(null,this)}
                                                        onChangeCallFun={this.callOnChange}
                                                        typeOfControl={meta?meta.Control:''}  
                                                        val={col[1]}
                                                    />
                                                </td>);*/}
        });

        if (nodes && this.props.multipleSelectionSettings && this.props.multipleSelectionSettings.isMultipleSelection) {
            var selectedRowIds = this.props.multipleSelectionSettings.getSelectedRowIds();
            //debugger;
            nodes.unshift(
                <td key="selection" style={columnStyles}>
                    <input
                        type="checkbox"
                        checked={this.props.multipleSelectionSettings.getIsRowChecked(dataView) }
                        onChange={this.handleSelectionChange}
                        ref="selected" />
                </td>
            );
        }
        //Get the row from the row settings.
        var className = that.props.rowSettings && that.props.rowSettings.getBodyRowMetadataClass(that.props.data) || "standard-row";

        if (that.props.isChildRow) {
            className = "child-row";
        } else if (that.props.hasChildren) {
            className = that.props.showChildren ? this.props.parentRowExpandedClassName : this.props.parentRowCollapsedClassName;
        }
        /* return (<tr 
                    onClick={this.props.multipleSelectionSettings && this.props.multipleSelectionSettings.isMultipleSelection ? this.handleSelectClick : null} 
                    className={className}>
                    {nodes}
                </tr>); */
        //  debugger;
        console.log(nodes);
        nodes = nodes.filter(function (n) { return n != null });
        console.log(this.props.childRowTemplate);

        let ClonedRowTemplate = React.cloneElement(this.props.childRowTemplate, {
            onClickFun: this.props.multipleSelectionSettings && this.props.multipleSelectionSettings.isMultipleSelection ? this.handleSelectClick.bind(this) : null,
            data: nodes,
            classNameProp: className,
            displayField: this.state.displayField
        }
        );
        return   ClonedRowTemplate;
            
    }
});


module.exports = GridRow;
