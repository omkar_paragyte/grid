let React = require('react');
let Inputval = React.createClass({
    getInitialState: function () {
        return {val : null,inputControl:<div />,data:''}
    },
    getDefaultProps: function(){
        return {
            "onChangeHandler": null,
            "typeOfControl":"textbox"
        }
    },
    componentWillMount: function() {
        debugger;
        this.setState({ val: this.props.val ,
                        inputControl: this.props.typeOfControl!=="checkbox"?
                                        (this.props.enableInlineEditing?
                                        <label onDoubleClick={this.ondblClick.bind(this)}>
                                            {this.props.val}
                                        </label>
                                        :
                                        <label>{this.props.val}</label>)
                                    :
                                    (this.props.enableInlineEditing?
                                     <ControlFactory
                                        typeOfControl={this.props.typeOfControl} 
                                        onBlur={this.onBlur.bind(this)} 
                                        val={this.props.val} 
                                        onChangeCallFun={this.props.onChangeCallFun}
                                        
                                        />
                                        :
                                        <input type="checkbox" checked={this.props.val!="false"&&this.props.val?true:false} />
                                        )
                        });
    },
    onChange: function (e) {
        debugger;
        console.log(e.target)
        this.setState({ val: e.target.value ,data:e});
        this.setState({ inputControl:<ControlFactory typeOfControl={this.props.typeOfControl} onBlur={this.onBlur.bind(this)} val={e.target.value} onChange={this.onChange.bind(event)} onDoubleClick={this.ondblClick.bind(this)}  />});
        /**<input  style={{border:"0px"}} onBlur={this.onBlur.bind(this)}  onDoubleClick={this.ondblClick.bind(this)} value={e.target.value} onChange={this.onChange.bind(this)}/>}); */    
    },
    ondblClick(e){
       this.setState({  inputControl:<ControlFactory typeOfControl={this.props.typeOfControl} val={this.state.val} onChange={this.onChange.bind(event)} onBlur={this.onBlur.bind(this)} />                                  
                        });
                        {/*<input  
                                            style={{border:"0px"}} 
                                            onBlur={this.onBlur.bind(this)}                                             
                                            value={this.state.val} 
                                            onChange={this.onChange.bind(event)}
                                            />
                                            */}
    
    },
    onBlur (e){
        debugger;
        console.log(this.state.data);
        this.props.typeOfControl!=="checkbox"?
        this.setState({  inputControl: <label onDoubleClick={this.ondblClick.bind(this)}>{this.state.val}</label>}):'';
        this.props.onChangeCallFun?this.props.onChangeCallFun(this):'';
        //this.props.onChangeHandler?this.props.onChangeHandler(this):'';
    },
    render: function () {
        return this.state.inputControl
    }
});
module.exports = Inputval;



let ControlFactory=React.createClass({
    getInitialState: function () {
        return { typeOfControl: "textbox",val:''}
    },
    getDefaultProps: function(){
        return {
            "val":'',
            "typeOfControl":"textbox",
            "onBlur":null,
            "onChange":null,
            "onDoubleClick":null
        }
    },
    componentWillUpdate:function(){
        console.log("update"+this.state.val)
    },
    componentWillMount: function() {
        debugger;
        this.setState({val:this.props.val});
    },
    getInputcontrol:function(){
        let inputControl;
         switch(this.props.typeOfControl){
            case "textbox":
                inputControl=<input type="text" style={{border:"0px"}}  onMouseOut={this.props.onBlur} onChange={this.props.onChange} onDoubleClick={this.props.onDoubleClick}  value={this.state.val} />
            
            break;
            case "checkbox":
                inputControl=<input type="checkbox"  onChange={this.onChange} checked={this.state.val&&this.state.val!="false"?true:false} />
            break;
        }
        return inputControl;
    },
    onChange:function(e){
        debugger;
        this.setState({val:e.target.checked});
        //console.log(this.state.val);
        //this.props.onChangeCallFun?this.props.onChangeCallFun(this):'';
    },
    render:function(){
        console.log(this.state.val);
        return this.getInputcontrol();
    }
});