let React = require('react');
let Inputval = React.createClass({
    getInitialState: function () {
        return {val : null,inputControl:<div />}
    },
    getDefaultProps: function(){
        return {
            "onChangeHandler": null,
            "typeOfControl":"textbox"
        }
    },
    componentWillMount: function() {
        debugger;
        this.setState({ val: this.props.val}); 
                    this.setState({    inputControl: this.props.typeOfControl!=="checkbox"?
                                        (this.props.enableInlineEditing?
                                        <label onDoubleClick={this.ondblClick.bind(this)}>
                                            {this.props.val}
                                        </label>
                                        :
                                        <label>{this.props.val}</label>)
                                    :
                                    this.props.enableInlineEditing?
                                     <input type="checkbox"  
                                            onChange={this.onChange.bind(this)} 
                                            checked={this.props.val&&this.props.val!="false"?true:false} 
                                            />
                                     :
                                        <input type="checkbox" 
                                               checked={this.props.val!="false"&&this.props.val?true:false} 
                                               />
                                     
                                    
                                        
                        });
    },
    onChange: function (e) {
        debugger;
        this.props.typeOfControl=="checkbox"?
        (this.setState({val:e.target.checked}),
           this.setState({ inputControl:<input type="checkbox"  
                                            onChange={this.onChange} 
                                            onMouseLeave={this.onBlur}
                                            checked={e.target.checked} />
        })):
       
    
        
        (this.setState({ val: e.target.value}),
        this.setState({ inputControl:<input type="text" 
                                            onMouseLeave={this.onBlur}  
                                            value={e.target.value} 
                                            onChange={this.onChange} 
                                            onDoubleClick={this.ondblClick}    />
        }));
         
    },
    ondblClick(e){
       this.setState({  inputControl: <input 
                                        type="text" 
                                        value={this.state.val} 
                                        onChange={this.onChange} 
                                        onMouseLeave={this.onBlur}
                                        /> 
                    });
    },
    onBlur (e){
        debugger;
        console.log(this.state.val);
        this.props.typeOfControl!=="checkbox"?
        this.setState({  inputControl: <label onDoubleClick={this.ondblClick.bind(this)}>{this.state.val}</label>}):'';
        this.props.onChangeCallFun?this.props.onChangeCallFun(this):'';
        
    },
    render: function () {
        return this.state.inputControl
    }
});
module.exports = Inputval;

